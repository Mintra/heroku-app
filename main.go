package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	igc "github.com/marni/goigc"
)

var startTime time.Time
var tracks []igc.Track
var lastID int

type track struct {
	HDate       time.Time `json:"h_date"`
	Pilot       string    `json:"pilot"`
	Glider      string    `json:"glider"`
	GliderID    string    `json:"glider_id"`
	TrackLength float64   `json:"track_length"`
}

type info struct {
	Uptime  string `json:"uptime"`
	Info    string `json:"info"`
	Version string `json:"version"`
}

// this makes it so that /igc/ and /igc are the same
func noEmptyString(array []string) []string {
	var returnArray []string

	for _, str := range array {
		if str != "" {
			returnArray = append(returnArray, str)
		}
	}
	return returnArray
}

// formats time.Duration to a string according to the ISO8601 standard
func formatISO8601(t time.Duration) string {
	seconds := int64(t.Seconds()) % 60 // You get total time for the fields
	minutes := int64(t.Minutes()) % 60
	hours := int64(t.Hours()) % 24

	totalHours := int64(t.Hours())
	days := (totalHours / 24) % 30 // half works, because this takes in account only 30 days
	months := (totalHours / (24 * 30)) % 12
	years := totalHours / (24 * 30 * 12)

	return fmt.Sprint("P", years, "Y", months, "M", days, "DT", hours, "H", minutes, "M", seconds, "S")
}

// handler for /igcinfo/api
func handlerAPI(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		parts := noEmptyString(strings.Split(r.URL.Path, "/"))
		if len(parts) == 2 {
			w.Header().Set("content-type", "application/json")
			var apiInfo info
			apiInfo.Uptime = formatISO8601(time.Since(startTime))
			apiInfo.Info = "service for IGC track"
			apiInfo.Version = "V1"
			json.NewEncoder(w).Encode(&apiInfo)
		} else {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		}
	default:
		http.Error(w, http.StatusText(http.StatusNotImplemented), http.StatusNotImplemented)
	}
}

// handler for /igcinfo/api/igc/<ID> and /igcinfo/api/igc/<id>/<field>
func handlerIgcIDField(w http.ResponseWriter, r *http.Request) {
	parts := noEmptyString(strings.Split(r.URL.Path, "/"))

	switch r.Method {
	case "GET":
		id, errur := strconv.Atoi(parts[3])
		if errur != nil { // if it isn't int
			http.Error(w, "Invalid ID type", http.StatusConflict)
			return
		}

		/* We're using map instead slice because it's very easy to see if it exists.
		due to getting a boolea calue and the object from the map*/
		if id < lastID { // we don't exceed
			Track := tracks[id]
			Track.Task.Start = Track.Points[0] // setting the points
			Track.Task.Finish = Track.Points[len(Track.Points)-1]
			Track.Task.Turnpoints = Track.Points[1 : len(Track.Points)-1]

			trackInfo := track{ // settnting the relevant data to an object
				HDate:       Track.Header.Date,
				Pilot:       Track.Header.Pilot,
				Glider:      Track.Header.GliderType,
				GliderID:    Track.Header.GliderID,
				TrackLength: Track.Task.Distance(),
			}
			if len(parts) == 4 { // recieve all the data of an ID
				w.Header().Set("content-type", "application/json")
				json.NewEncoder(w).Encode(trackInfo)
			} else { // send back data fromthe given field
				w.Header().Set("content-type", "text/plain")
				jsonString, _ := json.Marshal(trackInfo) // convert to json

				var field map[string]interface{}   // create map out of json
				json.Unmarshal(jsonString, &field) //unmarshal it to a map

				if res, found := field[parts[4]]; found {
					fmt.Fprintln(w, res)
				} else {
					http.Error(w, "Invalid field input", http.StatusNotFound)
				}
			}

		} else { // was not found
			http.Error(w, "ID exceeds size", http.StatusNotFound)
		}
	default:
		http.Error(w, http.StatusText(http.StatusNotImplemented), http.StatusNotImplemented)
	}
}

// handler for /igcinfo/api/igc
func handlerAPIIgc(w http.ResponseWriter, r *http.Request) {
	parts := noEmptyString(strings.Split(r.URL.Path, "/"))

	switch len(parts) {
	case 3:

		switch r.Method {

		case "POST": // adss new track,and returns the ID
			body, _ := ioutil.ReadAll(r.Body) // The whole body is read

			mapURL := make(map[string]string) // converts to a map
			json.Unmarshal(body, &mapURL)

			newTracks, errur := igc.ParseLocation(mapURL["url"]) // if the URL couldn't be parsed, the function stops
			if errur != nil {
				http.Error(w, "Invalid url input: ", http.StatusNotFound)
			}

			tracks = append(tracks, newTracks)

			data := make(map[string]int)
			data["id"] = lastID
			lastID++

			json.NewEncoder(w).Encode(data)

		case "GET":
			usedIDs := []int{}
			for i := 0; i < lastID; i++ { // returns the IDs
				usedIDs = append(usedIDs, i)
			}
			json.NewEncoder(w).Encode(usedIDs)

		default:
			http.Error(w, http.StatusText(http.StatusNotImplemented), http.StatusNotImplemented)
		}
	case 4, 5:
		handlerIgcIDField(w, r)

	default:
		http.Error(w, "Conflict", http.StatusConflict)
	}
}

func main() {
	lastID = 0

	startTime = time.Now()
	port, portOk := os.LookupEnv("PORT")
	if !portOk {
		port = "8080" // default port
	}

	fmt.Println("Port is:", port)

	http.HandleFunc("/igcinfo/api/igc/", handlerAPIIgc)

	http.HandleFunc("/igcinfo/api/", handlerAPI)

	http.HandleFunc("/igcinfo/", func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "Not allowed at /igcinfo.", http.StatusNotFound)
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "Not allowed at root.", http.StatusNotFound)
	})

	http.ListenAndServe(":"+port, nil)

}
