

This creates an API used to get and input information about IGC track


Links:
http://fathomless-bayou-76488.herokuapp.com/
https://gitlab.com/Mintra/heroku-app.git

/igcinfo/api/

GET: Used to retrun information about the API

/igcinfo/api/igc/

POST: If the given URL is valid, it'll add an IGC track to the API

GET: IDs that are currently in the memory will be returned

/igcinfo/api/igc/<ID>

GET: A numeric ID will be returned to you that starts from 1. This is the information about IDC track with the given ID.

igcinfo/api/igc/<ID>/<field>

GET: Returns the valid fields for the given ID. The fields are: H_date, pilot, glider, glider_id, track_length.

